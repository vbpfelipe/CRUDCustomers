package com.example.crud.felipevanderlei.crudcustomers.customer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.crud.felipevanderlei.crudcustomers.R;

import java.util.List;

/**
 * CustomerAdapter class links customers data and res.layout.item_list.xml parameters
 */

public class CustomerAdapter extends RecyclerView.Adapter<CustomerHolder> {

    private final List<Customer> customers;

    public CustomerAdapter(List<Customer> customers) {
        this.customers = customers;
    }

    /**
     * TO COMMENT!!!
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public CustomerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CustomerHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_list, parent, false));
    }

    /**
     * TO COMMENT!!!
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull CustomerHolder holder, int position) {
        holder.setCustomerName(customers.get(position).getName());
        final Customer customer = customers.get(position);

        //Catches button Delete Activities on RecyclerView
        holder.getDeleteBtn().setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view){
                final View v = view;
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle("Confirm")     //
                        .setMessage("Are you sure you want to delete this customer?")   //
                        .setPositiveButton("Delete",
                                new DialogInterface.OnClickListener() { //
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                CustomerDAO dao = new CustomerDAO(v.getContext());  //
                                try{    //
                                    //
                                    dao.deleteByID(customer.getId());
                                    //
                                    deleteCustomer(customer);
                                    //
                                    Toast.makeText(v.getContext(),
                                            "Customer was removed.",
                                            Toast.LENGTH_LONG).show();

                                }catch(Exception e){    //
                                    //
                                    Toast.makeText(v.getContext(),
                                            "ERROR: " + e.getMessage(),
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        }).setNegativeButton("Cancel", null)    //
                        .create()   //
                        .show();    //
            }
        });

        //Catches button Edit activities on RecyclerView
        //When clicked triggers a refresh on the current Activity
        holder.getEditBtn().setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view){
                Activity activity = getActivity(view);
                Intent intent =  activity.getIntent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("customer", customer);
                activity.finish();
                activity.startActivity(intent);
            }
        });
    }

    /**
     * TO COMMENT!!!
     * @return
     */
    @Override
    public int getItemCount() {
        return customers != null ? customers.size() : 0;
    }

    /**
     * Adds a new customer in collection in-memory and notifies RecyclerView to update.
     * @param customer
     */
    public void addCustomer(Customer customer){
        customers.add(customer);
        notifyItemInserted(getItemCount());
    }

    /**
     * Method permits to get an Activity through any View
     * since CustomerAdapter isn't an Activity.
     * @param view
     * @return
     */
    private Activity getActivity(View view){
        Context context = view.getContext();
        while(context instanceof ContextWrapper){
            if(context instanceof Activity){
                return (Activity)context;
            }

            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

    /**
     * Enables an easy and fast update on customers in RecycleView list.
     * @param customer
     */
    public void updateCustomer(Customer customer){
        customers.set(customers.indexOf(customer), customer);
        notifyItemChanged(customers.indexOf(customer));
    }

    /**
     * Removes a customer by a Customer Object.
     * Gets customer index, removes from the list and
     * notifies RecycleView.
     * @param customer
     */
    public void deleteCustomer(Customer customer){
        int position = customers.indexOf(customer);
        customers.remove(position);
        notifyItemRemoved(position);
    }
}
