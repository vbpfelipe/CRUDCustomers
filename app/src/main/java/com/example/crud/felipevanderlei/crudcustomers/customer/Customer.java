package com.example.crud.felipevanderlei.crudcustomers.customer;

import java.io.Serializable;

/**
 * Customer class represents Customers Table
 */

public class Customer implements Serializable {

    private int id;
    private String name;
    private String gender;
    private String state;
    private boolean vip;

    public Customer(int id, String name, String gender, String state, boolean vip) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.state = state;
        this.vip = vip;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getState() {
        return state;
    }

    public boolean isVip() {
        return vip;
    }

    @Override
    public boolean equals(Object o) {
        return this.id == ((Customer) o).id;
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}
