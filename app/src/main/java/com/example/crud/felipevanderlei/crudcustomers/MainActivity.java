package com.example.crud.felipevanderlei.crudcustomers;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.crud.felipevanderlei.crudcustomers.customer.Customer;
import com.example.crud.felipevanderlei.crudcustomers.customer.CustomerAdapter;
import com.example.crud.felipevanderlei.crudcustomers.customer.CustomerDAO;

/**
 * Main class of the project.
 * It's where everything it's executed and the applications happens.
 */
public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    private CustomerAdapter adapter;
    Customer editedCustomer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
        Verifies if this activity just started or if it came from an edition
        If it came from an edition (A Customer in the Intent who triggered this Activity),
        an edition screen is opened instead of that list screen.
        */
        Intent intent = getIntent();
        if(intent.hasExtra("customer")){
            findViewById(R.id.includemain).setVisibility(View.INVISIBLE);
            findViewById(R.id.includecadastro).setVisibility(View.VISIBLE);
            findViewById(R.id.fab).setVisibility(View.INVISIBLE);

            editedCustomer = (Customer) intent.getSerializableExtra("customer");

            EditText txtName = (EditText) findViewById(R.id.customerName);
            Spinner spnState = (Spinner) findViewById(R.id.stateSpn);
            CheckBox chkVip = (CheckBox) findViewById(R.id.vipChk);
            txtName.setText(editedCustomer.getName());
            chkVip.setChecked(editedCustomer.isVip());
            spnState.setSelection(getIndex(spnState, editedCustomer.getState()));
            if(editedCustomer.getGender() != null){
                RadioButton rb;
                if(editedCustomer.getGender().equals("M")){
                    rb = (RadioButton)findViewById(R.id.maleRB);
                }else{
                    rb = (RadioButton)findViewById(R.id.femaleRB);
                }
                rb.setChecked(true);
            }
        }

        //Intances Float Action Button and functionalities when activated.
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.includemain).setVisibility(View.INVISIBLE);
                findViewById(R.id.includecadastro).setVisibility(View.VISIBLE);
                findViewById(R.id.fab).setVisibility(View.INVISIBLE);
            }
        });

        //Instances Cancel Button from registration form and his functions.
        Button cancel = (Button) findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.includemain).setVisibility(View.VISIBLE);
                findViewById(R.id.includecadastro).setVisibility(View.INVISIBLE);
                findViewById(R.id.fab).setVisibility(View.VISIBLE);
            }
        });

        //Instances Save Button from registration form and functions.
        Button save = (Button) findViewById(R.id.btnSave);
        save.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                //Loading parameters section
                EditText txtName = (EditText) findViewById(R.id.customerName);
                RadioGroup genderRG = (RadioGroup) findViewById(R.id.genderRg);
                Spinner stateSpn = (Spinner) findViewById(R.id.stateSpn);
                CheckBox vipCheckB = (CheckBox) findViewById(R.id.vipChk);

                //Matching attributes section
                String name = txtName.getText().toString();
                String gender = genderRG.getCheckedRadioButtonId()  == R.id.maleRB ? "M" : "F";
                String state = stateSpn.getSelectedItem().toString();
                boolean vip = vipCheckB.isChecked();

                //saving data section
                CustomerDAO dao = new CustomerDAO(getBaseContext());
                try {

                    //Explicar
                    if (editedCustomer != null)
                        dao.save(editedCustomer.getId(), name, gender, state, vip);
                    else
                        dao.save(name, gender, state, vip);

                    //Get the last element and adds him in adapter
                    Customer customer = dao.getLast();

                    // Explicar
                    if (editedCustomer != null){
                        adapter.updateCustomer(customer);
                        editedCustomer = null;
                    }
                    else
                        adapter.addCustomer(customer);

                    //clean parameters
                    txtName.setText("");
                    genderRG.setSelected(false);
                    stateSpn.setSelection(0);
                    vipCheckB.setChecked(false);

                    //Shows on screen when Save Button is activated.
                    Toast.makeText(getApplicationContext(),"Saving...", Toast.LENGTH_LONG).
                            show();
                    findViewById(R.id.includemain).setVisibility(View.VISIBLE);
                    findViewById(R.id.includecadastro).setVisibility(View.INVISIBLE);
                    findViewById(R.id.fab).setVisibility(View.VISIBLE);

                    recyclerViewConfig();

                }catch(Exception e){
                    Toast.makeText(MainActivity.this, "ERROR: " + e.getMessage(), Toast.LENGTH_LONG).
                            show();
                }
            }
        });

        recyclerViewConfig();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * recyclerViewConfig() method populates at start the RecyclerView
     * with the database customers list.
     */
    private void recyclerViewConfig(){

        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);

        // Configures layout manager to be a list
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // Adds an adapter which will attach objects to a list
        CustomerDAO dao = new CustomerDAO(this);
        adapter = new CustomerAdapter(dao.listAll());
        recyclerView.setAdapter(adapter);

        // Break line.
        recyclerView.addItemDecoration(new DividerItemDecoration(
                this, DividerItemDecoration.VERTICAL)
        );
    }

    /**
     * Returns a parameter which will reference the element, Customer, that will be edited.
     * @param spinner
     * @param myString
     * @return
     */
    private int getIndex(Spinner spinner, String myString){
        int index = 0;
        for(int i=0; i<spinner.getCount(); i++){
            if(spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }
        return index;
    }
}
