package com.example.crud.felipevanderlei.crudcustomers.customer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.example.crud.felipevanderlei.crudcustomers.database.DatabaseGateway;

import java.util.ArrayList;
import java.util.List;

/**
 * Translates objects to databases, and vice-versa.
 * Abstracts data access.
 */
public class CustomerDAO {

    private static final String TABLE_CUSTOMERS = "TB_CUSTOMERS";
    private DatabaseGateway gw;

    public CustomerDAO(Context context){
        gw = DatabaseGateway.getInstance(context);
    }

    /**
     * Method "save" inserts new elements on CUSTOMERS table in CRUD Database.
     * @param name
     * @param gender
     * @param state
     * @param vip
     */
    public void save(String name, String gender, String state, boolean vip){
        save(0, name, gender, state, vip);
    }

    /**
     * Method "save" to insert new elements, or updates them,
     * on CUSTOMERS table in CRUD Database.
     * @param id
     * @param name
     * @param gender
     * @param state
     * @param vip
     */
    public void save(int id, String name, String gender, String state, boolean vip){
        ContentValues values = new ContentValues();
        values.put("CUS_NAME", name);
        values.put("CUS_GENDER", gender);
        values.put("CUS_STATE", state);
        values.put("CUS_VIP", vip);

        if(id > 0){
            gw.getDatabase().update(TABLE_CUSTOMERS, values, "CUS_ID=?", new String[] {id + ""});
        }else{
            gw.getDatabase().insert(TABLE_CUSTOMERS, null, values);
        }
    }

    /**
     * Method "listAll" returns all customers in TB_CUSTOMERS table in CRUD Database
     * @return customers
     */
    public List<Customer> listAll(){
        List<Customer> customers= new ArrayList<>();

        //.rawQuery executes a query using, or no, some parameters
        Cursor cursor = gw.getDatabase().rawQuery("SELECT * FROM " + TABLE_CUSTOMERS, null);

        //cursor.moveToNext() return true if there's a next item different than null
        while(cursor.moveToNext()){
            int id = cursor.getInt(cursor.getColumnIndex("CUS_ID"));
            String name = cursor.getString(cursor.getColumnIndex("CUS_NAME"));
            String gender = cursor.getString(cursor.getColumnIndex("CUS_GENDER"));
            String state = cursor.getString(cursor.getColumnIndex("CUS_STATE"));
            boolean vip = cursor.getInt(cursor.getColumnIndex("CUS_VIP")) > 0;

            customers.add(new Customer(id,name,gender,state,vip));
        }
        cursor.close();

        return customers;
    }

    /**
     * "getLast" method does a search for the last element from Customers Table
     * which is the most recent element added to this table.
     * @return
     */
    public Customer getLast(){
        Cursor cursor = gw.getDatabase().rawQuery("SELECT * FROM " + TABLE_CUSTOMERS +
                " ORDER BY CUS_ID DESC", null);
        if(cursor.moveToFirst()){
            int id = cursor.getInt(cursor.getColumnIndex("CUS_ID"));
            String name = cursor.getString(cursor.getColumnIndex("CUS_NAME"));
            String gender = cursor.getString(cursor.getColumnIndex("CUS_GENDER"));
            String state = cursor.getString(cursor.getColumnIndex("CUS_STATE"));
            boolean vip = cursor.getInt(cursor.getColumnIndex("CUS_VIP")) > 0;
            cursor.close();
            return new Customer(id, name, gender, state, vip);
        }

        return null;
    }

    /**
     * Method removes a customer by id.
     * @param id
     * @return
     */
    public boolean deleteByID(int id){
        return gw.getDatabase().delete(TABLE_CUSTOMERS,
                "CUS_ID=?",
                new String[]{id + ""}) > 0;
    }
}
