package com.example.crud.felipevanderlei.crudcustomers.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * DatabaseGateway represents Customer database that will do conections
 */
public class DatabaseGateway {

    private static DatabaseGateway gw;
    private SQLiteDatabase db;

    public DatabaseGateway(Context context) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        db = databaseHelper.getWritableDatabase();
        //.getWritableDatabase() creates and/or open a database that will be used
        // for reading and writing.
    }

    public static DatabaseGateway getInstance(Context context){
        if(gw == null)
            gw = new DatabaseGateway(context);

        return gw;
    }

    public SQLiteDatabase getDatabase(){
        return this.db;
    }
}
