package com.example.crud.felipevanderlei.crudcustomers.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Helps on creation and updates on a database
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "DB_CRUD";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_NAME = "TB_CUSTOMERS";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Method is executed automatically when a conection
     * with a database is made.
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME +
                "(CUS_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "CUS_NAME TEXT NOT NULL," +
                "CUS_GENDER TEXT," +
                "CUS_STATE TEXT NOT NULL," +
                "CUS_VIP INTEGER NOT NULL);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
